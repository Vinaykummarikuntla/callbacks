module.exports.callback6 = (boardID, listIDs, getBoardInfo, getLists, getCards) => {
    const callback1 = require('./callback1').callback1;
    const callback2 = require('./callback2').callback2;
    const callback3 = require('./callback3').callback3;

    setTimeout(() => {callback1(boardID, getBoardInfo)}, 0);
    setTimeout(() => {callback2(boardID, getLists)}, 0);
    setTimeout(() => {
        listIDs
            .forEach(listID => {
                callback3(listID, getCards);
            });
    }, 0);
}